import qbs
import qbs.File
import qbs.ModUtils
import qbs.TextFile
import qbs.FileInfo


Project {


    property string appPath: "bin";
    property string libPath : appPath;


    references: [
        "include/include.qbs",
        "code/code.qbs",
        "contrib/contrib.qbs",
        "test/test.qbs",
        "tools/tools.qbs"
    ]

}
