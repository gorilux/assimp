import qbs



Product {

    name: "assimp_cmd"

    Depends { name : "cpp" }    
    Depends { name: "assimp" }

    cpp.cxxLanguageVersion: "c++11";
    cpp.cxxStandardLibrary: "libstdc++";
    cpp.cxxFlags: ["-Wno-unused-parameter"]

    type: ["application"]
    //cpp.defines: ["_USE_MATH_DEFINES", "ASSIMP_BUILD_NO_C4D_IMPORTER"]


    files: [
        "CompareDump.cpp",
        "Export.cpp",
        "ImageExtractor.cpp",
        "Info.cpp",
        "Main.cpp",
        "Main.h",
        "WriteDumb.cpp",
        "generic_inserter.hpp",
        "resource.h",
    ]


    Group {
        name: "Exec install"
        fileTagsFilter: "application"
        qbs.install: true
        qbs.installDir: project.appPath
    }


}

