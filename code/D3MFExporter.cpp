/*
Open Asset Import Library (assimp)
----------------------------------------------------------------------

Copyright (c) 2006-2016, assimp team
All rights reserved.

Redistribution and use of this software in source and binary forms,
with or without modification, are permitted provided that the
following conditions are met:

* Redistributions of source code must retain the above
  copyright notice, this list of conditions and the
  following disclaimer.

* Redistributions in binary form must reproduce the above
  copyright notice, this list of conditions and the
  following disclaimer in the documentation and/or other
  materials provided with the distribution.

* Neither the name of the assimp team, nor the names of its
  contributors may be used to endorse or promote products
  derived from this software without specific prior
  written permission of the assimp team.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

----------------------------------------------------------------------
*/


#ifndef ASSIMP_BUILD_NO_3MF_EXPORTER

#include "D3MFExporter.h"

#include "fast_atof.h"
#include "SceneCombiner.h"
#include "DefaultIOSystem.h"

#include "D3MFXmlSerializer.h"
#include "../include/assimp/IOSystem.hpp"
#include "../include/assimp/Exporter.hpp"
#include "../include/assimp/scene.h"

#include "Exceptional.h"
#include "D3MFOpcPackage.h"


#include <ctime>
#include <set>




namespace Assimp {

void ExportScene3MF(const char* pFile, IOSystem* pIOSystem, const aiScene* pScene, const ExportProperties* pProperties)
{
    std::shared_ptr<IOStream> outfile (pIOSystem->Open(pFile, "wb"));
    if(!outfile)
    {
        throw DeadlyExportError("Could not open output .3mf file: " + std::string(pFile));
    }

    D3MF::D3MFOpcPackageWriter opcPackage(pIOSystem, pFile);

    D3MFExporter exporter(pScene, opcPackage.ModelStream());





    //bool shortened = false;
    //AssxmlExport::WriteDump( pScene, out, shortened );
    // invoke the exporter
//    STLExporter exporter(pFile, pScene);

//    // we're still here - export successfully completed. Write the file.
//    std::unique_ptr<IOStream> outfile (pIOSystem->Open(pFile,"wt"));
//    if(outfile == NULL) {
//        throw DeadlyExportError("could not open output .stl file: " + std::string(pFile));
//    }

    //outfile->Write( exporter.mOutput.str().c_str(), static_cast<size_t>(exporter.mOutput.tellp()),1);


}


D3MFExporter::D3MFExporter(const aiScene* pScene, IOStream* stream)
    : D3MF::XmlExporter(pScene, stream)
{

}

D3MFExporter::~D3MFExporter()
{

}

}


#endif
