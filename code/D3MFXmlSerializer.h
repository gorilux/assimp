////////////////////////////////////////////////////////////////////////////////
/// @brief
///
/// @file
///
/// DISCLAIMER
///
/// Copyright 2015 David Salvador Pinheiro
///
/// Licensed under the Apache License, Version 2.0 (the "License");
/// you may not use this file except in compliance with the License.
/// You may obtain a copy of the License at
///
///     http://www.apache.org/licenses/LICENSE-2.0
///
/// Unless required by applicable law or agreed to in writing, software
/// distributed under the License is distributed on an "AS IS" BASIS,
/// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
/// See the License for the specific language governing permissions and
/// limitations under the License.
///
/// Copyright holder is David Salvador Pinheiro
///
/// @author David Salvador Pinheiro
/// @author Copyright 2015, David Salvador Pinheiro
////////////////////////////////////////////////////////////////////////////////
#ifndef D3MFXMLSERIALIZER_H
#define D3MFXMLSERIALIZER_H


#include <memory>
#include <stack>
#include <ostream>

#include "../include/assimp/scene.h"

#include "D3MFOpcPackage.h"

namespace Assimp {

namespace D3MF {

class XmlImporter
{
public:
    XmlImporter(aiScene* scene);
    ~XmlImporter();

    void ImportXml(IOStream* stream);    

private:
    std::pair<int,aiNode*> ReadObject(XmlReader* xmlReader);

    aiMesh* ReadMesh(XmlReader* xmlReader);

    void ImportVertices(XmlReader* xmlReader, aiMesh* mesh);
    aiVector3D ReadVertex(XmlReader* xmlReader);

    void ImportTriangles(XmlReader* xmlReader, aiMesh* mesh);

    aiFace ReadTriangle(XmlReader* xmlReader);

    std::pair<unsigned long, aiMatrix4x4> ReadItem(XmlReader* xmlReader);

    bool ReadToEndElement(XmlReader* xmlReader, const std::string& closeTag);

private:
    std::vector<aiMesh*> meshes;
    aiScene* m_Scene;


};

class IOStreamBuf;

class XmlExporter
{
public:
    XmlExporter( const aiScene* scene, IOStream* stream);
    virtual ~XmlExporter();

private:
    void StartElement(const std::string& element, const std::map<std::string, std::string>& attributes);
    void EndElement();

    void WriteNode(const aiNode* node);
    void WriteMesh(const aiMesh* mesh);
private:
    const aiScene* m_Scene;
    std::unique_ptr<IOStreamBuf> m_StreamBuf;
    std::ostream m_XmlOStream;
    unsigned long objectId;

};




}
}

#endif // D3MFXMLSERIALIZER_H
