////////////////////////////////////////////////////////////////////////////////
/// @brief
///
/// @file
///
/// DISCLAIMER
///
/// Copyright 2015 David Salvador Pinheiro
///
/// Licensed under the Apache License, Version 2.0 (the "License");
/// you may not use this file except in compliance with the License.
/// You may obtain a copy of the License at
///
///     http://www.apache.org/licenses/LICENSE-2.0
///
/// Unless required by applicable law or agreed to in writing, software
/// distributed under the License is distributed on an "AS IS" BASIS,
/// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
/// See the License for the specific language governing permissions and
/// limitations under the License.
///
/// Copyright holder is David Salvador Pinheiro
///
/// @author David Salvador Pinheiro
/// @author Copyright 2015, David Salvador Pinheiro
////////////////////////////////////////////////////////////////////////////////
#include "D3MFXmlSerializer.h"

#include <string>
#include <sstream>
#include <vector>
#include <map>
#include <algorithm>
#include <cassert>
#include <cstdlib>
#include <memory>
#include <sstream>

#include "../include/assimp/ai_assert.h"
#include "../include/assimp/DefaultLogger.hpp"

#include "XMLTools.h"



namespace Assimp {

namespace D3MF {


namespace XmlTag {

    const std::string model     = "model";
    const std::string metadata  = "metadata";
    const std::string resources = "resources";
    const std::string object    = "object";
    const std::string components = "components";
    const std::string component = "component";
    const std::string mesh      = "mesh";
    const std::string vertices  = "vertices";
    const std::string vertex    = "vertex";
    const std::string triangles = "triangles";
    const std::string triangle  = "triangle";
    const std::string x         = "x";
    const std::string y         = "y";
    const std::string z         = "z";
    const std::string v1        = "v1";
    const std::string v2        = "v2";
    const std::string v3        = "v3";
    const std::string id        = "id";
    const std::string name      = "name";
    const std::string type      = "type";
    const std::string build     = "build";
    const std::string item      = "item";
    const std::string objectid  = "objectid";
    const std::string transform = "transform";

}

namespace Utils{

std::vector<std::string> Split(const std::string& str, char delimiter)
{
    std::vector<std::string> result;
    std::stringstream ss(str);
    std::string tok;


    while(std::getline(ss, tok, delimiter)) {
       result.push_back(tok);
     }

    return result;
}

}



XmlImporter::XmlImporter(aiScene* scene)
    : m_Scene(scene)
{
    m_Scene->mFlags |= AI_SCENE_FLAGS_NON_VERBOSE_FORMAT;
}




XmlImporter::~XmlImporter()
{

}

void XmlImporter::ImportXml(IOStream* stream)
{

    std::unique_ptr<CIrrXML_IOStreamReader> xmlStream(new CIrrXML_IOStreamReader(stream));
    std::unique_ptr<D3MF::XmlReader> xmlReader(irr::io::createIrrXMLReader(xmlStream.get()));

    m_Scene->mRootNode = new aiNode();
    std::map<unsigned long,aiNode*> children;
    std::map<unsigned long, aiMatrix4x4> transform;

    while(ReadToEndElement(xmlReader.get(), D3MF::XmlTag::model))
    {

        if(xmlReader->getNodeName() == D3MF::XmlTag::object)
        {
            children.insert(ReadObject(xmlReader.get()));
        }
        else if(xmlReader->getNodeName() == D3MF::XmlTag::item)
        {
            transform.insert(ReadItem(xmlReader.get()));
        }
    }

    if(m_Scene->mRootNode->mName.length == 0)
        m_Scene->mRootNode->mName.Set("3MF");


    m_Scene->mNumMeshes = static_cast<unsigned int>(meshes.size());
    m_Scene->mMeshes = new aiMesh*[m_Scene->mNumMeshes]();

    std::copy(meshes.begin(), meshes.end(), m_Scene->mMeshes);

    m_Scene->mRootNode->mNumChildren = static_cast<unsigned int>(children.size());
    m_Scene->mRootNode->mChildren = new aiNode*[m_Scene->mRootNode->mNumChildren]();

    auto destination = m_Scene->mRootNode->mChildren;
    std::for_each(children.begin(), children.end(), [&destination, &transform]( std::pair<unsigned long, aiNode*> const& node)
    {

        auto transformItr = transform.find(node.first);

        if(transformItr != transform.end())
        {
            node.second->mTransformation = transformItr->second;
        }

        *destination = node.second;
        destination++;
    });

}


std::pair<int, aiNode*> XmlImporter::ReadObject(XmlReader* xmlReader)
{
    ScopeGuard<aiNode> node(new aiNode());

    std::vector<unsigned long> meshIds;

    unsigned long id = std::strtoul(xmlReader->getAttributeValue(D3MF::XmlTag::id.c_str()), NULL, 10);
    std::string name(xmlReader->getAttributeValue(D3MF::XmlTag::name.c_str()));
    std::string type(xmlReader->getAttributeValue(D3MF::XmlTag::type.c_str()));

    node->mParent = m_Scene->mRootNode;
    node->mName.Set(name);

    unsigned long meshIdx = meshes.size();

    while(ReadToEndElement(xmlReader, D3MF::XmlTag::object))
    {
        if(xmlReader->getNodeName() == D3MF::XmlTag::mesh)
        {
            auto mesh = ReadMesh(xmlReader);

            mesh->mName.Set(name);
            meshes.push_back(mesh);
            meshIds.push_back(meshIdx);
            meshIdx++;

        }
    }

    node->mNumMeshes = static_cast<unsigned int>(meshIds.size());

    node->mMeshes = new unsigned int[node->mNumMeshes];

    std::copy(meshIds.begin(), meshIds.end(), node->mMeshes);

    return std::make_pair(id, node.dismiss());

}

aiMesh* XmlImporter::ReadMesh(XmlReader* xmlReader)
{
    aiMesh* mesh = new aiMesh();

    while(ReadToEndElement(xmlReader, D3MF::XmlTag::mesh))
    {
        if(xmlReader->getNodeName() == D3MF::XmlTag::vertices)
        {
            ImportVertices(xmlReader, mesh);
        }
        else if(xmlReader->getNodeName() == D3MF::XmlTag::triangles)
        {
            ImportTriangles(xmlReader, mesh);
        }

    }


    return mesh;
}

void XmlImporter::ImportVertices(XmlReader* xmlReader, aiMesh* mesh)
{
    std::vector<aiVector3D> vertices;

    while(ReadToEndElement(xmlReader, D3MF::XmlTag::vertices))
    {
        if(xmlReader->getNodeName() == D3MF::XmlTag::vertex)
        {
            vertices.push_back(ReadVertex(xmlReader));
        }
    }
    mesh->mNumVertices = static_cast<unsigned int>(vertices.size());
    mesh->mVertices = new aiVector3D[mesh->mNumVertices];

    std::copy(vertices.begin(), vertices.end(), mesh->mVertices);

}

aiVector3D XmlImporter::ReadVertex(XmlReader* xmlReader)
{
    aiVector3D vertex;
    vertex.x = std::strtof(xmlReader->getAttributeValue(D3MF::XmlTag::x.c_str()), nullptr);
    vertex.y = std::strtof(xmlReader->getAttributeValue(D3MF::XmlTag::y.c_str()), nullptr);
    vertex.z = std::strtof(xmlReader->getAttributeValue(D3MF::XmlTag::z.c_str()), nullptr);

    return vertex;
}

void XmlImporter::ImportTriangles(XmlReader* xmlReader, aiMesh* mesh)
{
    std::vector<aiFace> faces;


    while(ReadToEndElement(xmlReader, D3MF::XmlTag::triangles))
    {
        if(xmlReader->getNodeName() == D3MF::XmlTag::triangle)
        {
            faces.push_back(ReadTriangle(xmlReader));
        }
    }

    mesh->mNumFaces = static_cast<unsigned int>(faces.size());
    mesh->mFaces = new aiFace[mesh->mNumFaces];
    mesh->mPrimitiveTypes = aiPrimitiveType_TRIANGLE;


    std::copy(faces.begin(), faces.end(), mesh->mFaces);

}

aiFace XmlImporter::ReadTriangle(XmlReader* xmlReader)
{
    aiFace face;

    face.mNumIndices = 3;
    face.mIndices = new unsigned int[face.mNumIndices];
    face.mIndices[0] = static_cast<unsigned int>(std::atoi(xmlReader->getAttributeValue(D3MF::XmlTag::v1.c_str())));
    face.mIndices[1] = static_cast<unsigned int>(std::atoi(xmlReader->getAttributeValue(D3MF::XmlTag::v2.c_str())));
    face.mIndices[2] = static_cast<unsigned int>(std::atoi(xmlReader->getAttributeValue(D3MF::XmlTag::v3.c_str())));

    return face;
}

std::pair<unsigned long, aiMatrix4x4> XmlImporter::ReadItem(XmlReader* xmlReader)
{
    unsigned long objectid = std::strtoul(xmlReader->getAttributeValue(D3MF::XmlTag::objectid.c_str()), NULL, 10);
    aiMatrix4x4 matrix;

    auto transform = Utils::Split(xmlReader->getAttributeValue(D3MF::XmlTag::transform.c_str()), ' ');

    int idx = 0;
    for(int i = 0; i < 4; i++)
    {
        for(int j = 0; j < 3; j++)
        {
            matrix.m[i][j] = std::strtof(transform[idx++].c_str(), nullptr);
        }
    }
    matrix.m[0][3] = 0;
    matrix.m[1][3] = 0;
    matrix.m[2][3] = 0;
    matrix.m[3][3] = 1.0;

    return std::make_pair(objectid, matrix);
}

bool XmlImporter::ReadToEndElement(XmlReader* xmlReader, const std::string& closeTag)
{
    while(xmlReader->read())
    {
        if (xmlReader->getNodeType() == irr::io::EXN_ELEMENT) {
            return true;
        }
        else if (xmlReader->getNodeType() == irr::io::EXN_ELEMENT_END
                 && xmlReader->getNodeName() == closeTag)
        {
            return false;
        }
    }
    DefaultLogger::get()->error("unexpected EOF, expected closing <" + closeTag + "> tag");
    return false;
}


class IOStreamBuf : public std::streambuf
{
public:
    IOStreamBuf(IOStream* iostream)
        :m_IOStream(iostream){}
    virtual ~IOStreamBuf(){}


private:

    IOStream* m_IOStream;

    // basic_streambuf interface
protected:
    std::streamsize xsputn(const char_type* s, std::streamsize n) override
    {
        return m_IOStream->Write(s,sizeof(char_type),n);
    }
    int_type overflow(int_type c) override
    {
        if(c == EOF)

        return 1;
    }
};



XmlExporter::XmlExporter(const aiScene* scene, IOStream* stream)
    : m_Scene( scene ),
      m_StreamBuf( new IOStreamBuf(stream) ),
      m_XmlOStream(m_StreamBuf.get())
{
    if(!(m_Scene->mFlags & AI_SCENE_FLAGS_NON_VERBOSE_FORMAT))
    {

    }

    objectId = 1;

    WriteNode(m_Scene->mRootNode);

}

XmlExporter::~XmlExporter()
{

}

void XmlExporter::StartElement(const std::string& element, const std::map<std::string, std::string>& attributes)
{
    m_XmlOStream << "<" << element << " ";

    for(auto& att: attributes){
        m_XmlOStream  << att.first << "=\"" << att.second << "\"";
    };


}

void XmlExporter::EndElement()
{

}

void XmlExporter::WriteNode(const aiNode* node)
{
    for(size_t i = 0; i < node->mNumMeshes; ++i)
    {
        m_XmlOStream << "<" << XmlTag::object
                     << " " << XmlTag::id   << "=\"" <<  objectId++ << "\""
                     << " " << XmlTag::name << "=\"" <<  node->mName.C_Str() << "\""
                     << " " << XmlTag::type << "=\"model\">" << std::endl;
        WriteMesh(m_Scene->mMeshes[i]);

        m_XmlOStream <<"</" << XmlTag::object << ">";
    }
    //TODO: components
}

void XmlExporter::WriteMesh(const aiMesh* mesh)
{
    m_XmlOStream << "<" << XmlTag::mesh << ">"
                 << "<" << XmlTag::vertices << ">";

    for(size_t i = 0; i < mesh->mNumVertices; ++i)
    {
        m_XmlOStream << "<" << XmlTag::vertex
                     << XmlTag::x << "=\"" << mesh->mVertices[i].x << "\""
                     << XmlTag::y << "=\"" << mesh->mVertices[i].y << "\""
                     << XmlTag::z << "=\"" << mesh->mVertices[i].z << "\" "
                     << "/>";
    }

    m_XmlOStream << "</" << XmlTag::mesh << ">"
                 << "</" << XmlTag::vertices << ">";
}



}
}
