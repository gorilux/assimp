import qbs


Product {


    name: "openddlparser"

    Depends { name : "cpp" }

    cpp.cxxFlags: ["-Wno-unused-parameter", "-std=c++11"]

    cpp.includePaths : "include"

    //type: ["dynamiclibrary"]
    type: ["staticlibrary"]


    files: [
        "code/DDLNode.cpp",
        "code/OpenDDLCommon.cpp",
        "code/OpenDDLExport.cpp",
        "code/OpenDDLParser.cpp",
        "code/Value.cpp",
        "include/openddlparser/DDLNode.h",
        "include/openddlparser/OpenDDLCommon.h",
        "include/openddlparser/OpenDDLExport.h",
        "include/openddlparser/OpenDDLParser.h",
        "include/openddlparser/OpenDDLParserUtils.h",
        "include/openddlparser/Value.h"
    ]

    Export {
        Depends { name: "cpp" }
        cpp.includePaths : "include"
        cpp.cxxFlags: "-std=c++11"
    }
}
