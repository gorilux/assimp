import qbs


Project {

    references: [
        "clipper/clipper.qbs",
        "ConvertUTF/ConvertUTF.qbs",
        "irrXML/irrXML.qbs",
        "poly2tri/poly2tri.qbs",
        "rapidjson/rapidjson.qbs",
        "openddlparser/openddlparser.qbs",
        "unzip/unzip.qbs",
        "zlib/zlib.qbs"

    ]
}
