import qbs


Product {


    name : "clipper"

    Depends { name : "cpp" }

//    cpp.cxxStandardLibrary: "libstdc++";
    cpp.cxxFlags: ["-Wno-unused-parameter", "-std=c++11"]
//    cpp.defines: ["_USE_MATH_DEFINES", "ASSIMP_BUILD_NO_C4D_IMPORTER"]


    //type: ["dynamiclibrary"]
    type: ["staticlibrary"]


    files : [
        "clipper.cpp",
        "clipper.hpp",
    ]

    Export {
        Depends { name: "cpp" }
        cpp.includePaths : "."
    }


}
