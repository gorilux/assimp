import qbs


Product {

    name : "unzip"
    Depends { name : "cpp" }

    cpp.defines: ["ASSIMP_BUILD_NO_OWN_ZLIB"]


    //type: ["dynamiclibrary"]
    type: ["staticlibrary"]


    files : [
        "crypt.h",
        "ioapi.c",
        "ioapi.h",
        "unzip.c",
        "unzip.h",
        "zip.h",
        "zip.c"
    ]
    Export {
        Depends { name: "cpp" }
        cpp.includePaths : "."
    }

}
