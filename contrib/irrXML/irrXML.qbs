import qbs


Product {

    name: "irrXML"

    Depends { name : "cpp" }

    cpp.cxxFlags: ["-Wno-unused-parameter", "-std=c++11"]
//    cpp.defines: ["_USE_MATH_DEFINES", "ASSIMP_BUILD_NO_C4D_IMPORTER"]


    //type: ["dynamiclibrary"]
    type: ["staticlibrary"]

    files : [
        "CXMLReaderImpl.h",
        "heapsort.h",
        "irrArray.h",
        "irrString.h",
        "irrTypes.h",
        "irrXML.cpp",
        "irrXML.h",
    ]

    Export {
        Depends { name: "cpp" }
        cpp.includePaths : "."
        cpp.cxxFlags: "-std=c++11"
    }


}
