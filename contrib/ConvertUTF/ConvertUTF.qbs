import qbs


Product {

    name : "ConvertUTF"

    Depends { name : "cpp" }

//    cpp.cxxStandardLibrary: "libstdc++";
    cpp.cxxFlags: ["-Wno-unused-parameter"]
//    cpp.defines: ["_USE_MATH_DEFINES", "ASSIMP_BUILD_NO_C4D_IMPORTER"]


    //type: ["dynamiclibrary"]
    type: ["staticlibrary"]

    files : [
        "ConvertUTF.c",
        "ConvertUTF.h",
    ]

    Export {
        Depends { name: "cpp" }
        cpp.includePaths : "."
    }

}
