import qbs


Product {


    name : "zlib"
    Depends { name : "cpp" }

    cpp.cxxFlags: ["-Wno-unused-parameter"]
//    cpp.defines: ["_USE_MATH_DEFINES", "ASSIMP_BUILD_NO_C4D_IMPORTER"]


    //type: ["dynamiclibrary"]
    type: ["staticlibrary"]


    files : [
        "adler32.c",
        "compress.c",
        "crc32.c",
        "crc32.h",
        "deflate.c",
        "deflate.h",
        "gzclose.c",
        "gzguts.h",
        "gzlib.c",
        "gzread.c",
        "gzwrite.c",
        "infback.c",
        "inffast.c",
        "inffast.h",
        "inffixed.h",
        "inflate.c",
        "inflate.h",
        "inftrees.c",
        "inftrees.h",
        "trees.c",
        "trees.h",
        "uncompr.c",
        "zconf.h",
        "zlib.h",
        "zlib.pc",
        "zutil.c",
        "zutil.h",
    ]

    Export {
        Depends { name: "cpp" }
        cpp.includePaths : "."
    }
}
