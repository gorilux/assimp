import qbs


Product {

    name : "poly2tri"

    Depends { name : "cpp" }

//    cpp.
//    cpp.cxxStandardLibrary: "libstdc++";
    cpp.cxxFlags: ["-Wno-unused-parameter", "-std=c++11"]
//    cpp.defines: ["_USE_MATH_DEFINES", "ASSIMP_BUILD_NO_C4D_IMPORTER"]


    //type: ["dynamiclibrary"]
    type: ["staticlibrary"]

    files : [
        "poly2tri/common/shapes.cc",
        "poly2tri/common/shapes.h",
        "poly2tri/common/utils.h",
        "poly2tri/poly2tri.h",
        "poly2tri/sweep/advancing_front.cc",
        "poly2tri/sweep/advancing_front.h",
        "poly2tri/sweep/cdt.cc",
        "poly2tri/sweep/cdt.h",
        "poly2tri/sweep/sweep.cc",
        "poly2tri/sweep/sweep.h",
        "poly2tri/sweep/sweep_context.cc",
        "poly2tri/sweep/sweep_context.h",
    ]

    Export {
        Depends { name: "cpp" }
        cpp.includePaths : "."
    }


}
