import qbs


Project {


    property string modelsDir : project.sourceDirectory + "/test/models"

    references: [
        "unit/unit.qbs"
    ]

}
