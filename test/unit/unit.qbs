import qbs


Product {
    name: "unit"

    Depends { name : "cpp" }
    Depends { name: "assimp" }

    cpp.cxxLanguageVersion: "c++11";
    cpp.cxxStandardLibrary: "libstdc++";
    type: ["application"]


    cpp.defines: "ASSIMP_TEST_MODELS_DIR=\""+ project.modelsDir+"\""

     cpp.dynamicLibraries: "gtest"


    files: [
        "AssimpAPITest.cpp",
        "CCompilerTest.c",
        "Main.cpp",
        "UnitTestPCH.h",        
        "utColladaExportCamera.cpp",
        "utColladaExportLight.cpp",
        "utExport.cpp",
        "utFastAtof.cpp",
        "utFindDegenerates.cpp",
        "utFindInvalidData.cpp",
        "utFixInfacingNormals.cpp",
        "utGenNormals.cpp",
        "utIOSystem.cpp",
        "utImporter.cpp",
        "utImproveCacheLocality.cpp",
        "utIssues.cpp",
        "utJoinVertices.cpp",
        "utLimitBoneWeights.cpp",
        "utMaterialSystem.cpp",
        "utMatrix3x3.cpp",
        "utMatrix4x4.cpp",        
        "utPretransformVertices.cpp",
        "utRemoveComments.cpp",
        "utRemoveComponent.cpp",
        "utRemoveRedundantMaterials.cpp",
        "utScenePreprocessor.cpp",
        "utSharedPPData.cpp",
        "utSortByPType.cpp",
        "utSplitLargeMeshes.cpp",
        "utTargetAnimation.cpp",
        "utTextureTransform.cpp",
        "utTriangulate.cpp",
        "utVertexTriangleAdjacency.cpp",
    ]

    Group {
        name: "Exec install"
        fileTagsFilter: "application"
        qbs.install: true
        qbs.installDir: project.appPath
    }


}
