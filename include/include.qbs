import qbs


Product {

    name: "include"
    files : [
        "assimp/Compiler/poppack1.h",
        "assimp/Compiler/pstdint.h",
        "assimp/Compiler/pushpack1.h",
        "assimp/DefaultLogger.hpp",
        "assimp/Exporter.hpp",
        "assimp/IOStream.hpp",
        "assimp/IOSystem.hpp",
        "assimp/Importer.hpp",
        "assimp/LogStream.hpp",
        "assimp/Logger.hpp",
        "assimp/NullLogger.hpp",
        "assimp/ProgressHandler.hpp",
        "assimp/ai_assert.h",
        "assimp/anim.h",
        "assimp/camera.h",
        "assimp/cexport.h",
        "assimp/cfileio.h",
        "assimp/cimport.h",
        "assimp/color4.h",
        "assimp/color4.inl",
        "assimp/config.h",
        "assimp/defs.h",
        "assimp/importerdesc.h",
        "assimp/light.h",
        "assimp/material.h",
        "assimp/material.inl",
        "assimp/matrix3x3.h",
        "assimp/matrix3x3.inl",
        "assimp/matrix4x4.h",
        "assimp/matrix4x4.inl",
        "assimp/mesh.h",
        "assimp/metadata.h",
        "assimp/postprocess.h",
        "assimp/quaternion.h",
        "assimp/quaternion.inl",
        "assimp/scene.h",
        "assimp/texture.h",
        "assimp/types.h",
        "assimp/vector2.h",
        "assimp/vector2.inl",
        "assimp/vector3.h",
        "assimp/vector3.inl",
        "assimp/version.h",
    ]


    Export {
        Depends { name: "cpp" }
        cpp.includePaths : "."
    }


}
